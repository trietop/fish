from fish import LightController
import clg
import logging
import logging.config
import pytz
import yamlordereddictloader
import yaml
import time

def Date(value):
    from datetime import datetime
    try:
        return datetime.strptime(value, '%d/%m/%Y')
    except Exception as err:
        raise clg.argparse.ArgumentTypeError(err)
clg.TYPES['Date'] = Date

# Set up logging
logging.config.fileConfig('logging.conf')

# Parse those args
cmd = clg.CommandLine(yaml.load(open('args.yaml'), Loader=yamlordereddictloader.Loader))
args = cmd.parse()

# Read configuration file
config = yaml.load(open(args.config), Loader=yamlordereddictloader.Loader)

# Open controller
controller = LightController(config)
if args.dryrun:
    controller.emit = False

if args.date:
    controller.reference_date(args.date)
if args.plan:
    controller.plan(args.date)

if args.preview:
    controller.connect()
    controller.preview(args.delay)

if args.dryrun:
    exit(0)

# Start plan
while True:
    controller.run()
    time.sleep(30)
