from astral import Astral
from datetime import datetime, timedelta
from lirconian import UnixDomainSocketLirconian, TcpLirconian, ClientInstantiationError

import logging
import time
import pytz
import queue
import sched
import yaml
import yamlordereddictloader


DEFAULT_LIRC_DEVICE = '/var/run/lirc/lircd'
DEFAULT_PORT = 8765

def _new_lirconian(config):
    """
    Factory method that returns a concrete subclass of the Lirconian,
    depending on the argument.
    """
    try:
        return UnixDomainSocketLirconian(config['socket'] if 'socket' in config else DEFAULT_LIRC_DEVICE,
                                         config['verbose'] if 'verbose' in config else False,
                                         config['timeout'] if 'timeout' in config else None) \
            if config is None or not 'address' in config else \
            TcpLirconian(config['address'],
                         config['port'] if 'port' in config else 8765,
                         config['verbose'] if 'verbose' in config else False,
                         config['timeout'] if 'timeout' in config else None)
    except Exception as ex:
        raise ClientInstantiationError(ex)


class LightController:

    def __init__(self, config):
        self.log = logging.getLogger(__name__)
        self.keys = yaml.load(open('conf/keys.yaml'),
                              Loader=yamlordereddictloader.Loader)
        self.emit = True
        self.config = config
        self.astral = Astral()
        self.location = self.astral[config['city']]
        self.tzinfo = pytz.timezone(self.location.timezone)
        self.reference = datetime.now(self.tzinfo)
        self.q = queue.Queue()
        self.timer = sched.scheduler()

    def emit(self, emission):
        self.log.info("Emission: %s" % str(emission))
        self.emit = emission

    def reference_date(self, refdate):
        self.reference = self.tzinfo.localize(refdate, is_dst=None)

    def connect(self):
        self.lirc = _new_lirconian(self.config['lirc'] if 'lirc' in self.config else {})
        version = self.lirc.get_version()
        self.log.info("LIRC version: %s" % version)

    def plan(self, refdate=None):
        self.sun = self.location.sun(date=self.reference)
        if not refdate:
            now = datetime.now(self.tzinfo)
        else:
            now = self.tzinfo.localize(refdate, is_dst=None)

        delta = 0
        reftime = now

        if self.sun['dawn'] > now:
            diff = self.sun['dawn'] - reftime
            self.log.info('Dawn:    %s (%s)' %
                          (str(self.sun['dawn']), str(diff.total_seconds() / 60)))
            self.q.put(dict([("wait", diff.total_seconds() / 60)]))
            delta = self._add(self.config['plans']['default']['dawn'])
            reftime = self.sun['dawn']
        if self.sun['sunrise'] > now:
            diff = self.sun['sunrise'] - (reftime + timedelta(minutes=delta))
            self.log.info('Sunrise: %s (%s)' %
                          (str(self.sun['sunrise']), str(diff.total_seconds() / 60)))
            self.q.put(dict([("wait", diff.total_seconds() / 60)]))
            delta = self._add(self.config['plans']['default']['sunrise'])
            reftime = self.sun['sunrise']
        if self.sun['sunset'] > now:
            diff = self.sun['sunset'] - (reftime + timedelta(minutes=delta))
            self.log.info('Sunset:  %s (%s)' %
                          (str(self.sun['sunset']), str(diff.total_seconds() / 60)))
            self.q.put(dict([("wait", diff.total_seconds() / 60)]))
            delta = self._add(self.config['plans']['default']['sunset'])
            reftime = self.sun['sunset']
        diff = self.sun['dusk'] - (reftime + timedelta(minutes=delta))
        self.log.info('Dusk:    %s' % str(self.sun['dusk']))
        delta = self._add(self.config['plans']['default']['dusk'])
        self.log.info('End:     %s' % (self.sun['dusk'] + timedelta(minutes=delta)))

    def run(self):
        if not self.q.empty():
            self._tick()

    def preview(self, delay):
        self._tick(delay=delay)
        while not self.q.empty():
            self.timer.run()
            time.sleep(1)

    def _add(self, items):
        minutes = 0
        for item in items:
            if isinstance(item, dict):
                if 'wait' in item:
                    minutes += item['wait']
            self.q.put(item)
        return minutes

    def _tick(self, delay=None):
        try:
            while True:
                item = self.q.get_nowait()
                if item is None:
                    self.q.task_done()
                    break
                elif isinstance(item, dict):
                    if 'wait' in item:
                        if delay:
                            self.log.info("Delay of %s seconds (actual is: %s minutes)" %
                                          (str(delay), str(item['wait'])))
                            self.timer.enter(delay, 1, self._tick, argument=(delay,))
                        else:
                            self.log.info("Delay of %s minutes" % str(item['wait']))
                            self.timer.enter(item['wait'] * 60, 1, self._tick)
                    self.q.task_done()
                    break
                else:
                    self.log.info("Key press: " + item + ", key: " +
                                  self.keys['mapping'][item])
                    if self.emit:
                        self.lirc.send_ir_command(self.keys['remote'], self.keys['mapping'][item], 1)
                        time.sleep(0.5)
                self.q.task_done()
        except queue.Empty:
            exit(0)
